provider "aws" {
    region      = var.aws_region
    profile     = var.aws_profile
    access_key  = var.aws_access_key
    secret_key  = var.aws_secret_key
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "main_public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
}

resource "aws_subnet" "main_private" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-central-1a"
}

resource "aws_subnet" "main_private_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-central-1b"
}

# create security group for EC2
resource "aws_security_group" "dev_web" {
  name          = "web_node"
  description   = "DEV Security Group"
  vpc_id        = aws_vpc.main.id
  ingress {
    from_port   = var.web_port
    to_port     = var.web_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# create security group for mariadb
resource "aws_security_group" "allow_mariadb" {
  vpc_id            = aws_vpc.main.id
  name              = "allow_mariadb"
  ingress {
    from_port       = var.rds_port
    to_port         = var.rds_port
    protocol        = "tcp"
    security_groups = [aws_security_group.dev_web.id]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    self            = true
  }
}

# internet GW
resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main.id
}

# route tables
resource "aws_route_table" "main_public" {
  vpc_id        = aws_vpc.main.id
  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.main_gw.id
  }
}

# route associations public
resource "aws_route_table_association" "main_public" {
  subnet_id      = aws_subnet.main_public.id
  route_table_id = aws_route_table.main_public.id
}

#create ssh key
resource "aws_key_pair" "my_key" {
  key_name   = "my_key"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

# create dev instance
resource "aws_instance" "dev" {
  ami           = var.amis[var.aws_region]
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.main_private.id
  vpc_security_group_ids = [aws_security_group.dev_web.id]
  key_name      = aws_key_pair.my_key.key_name
}

# create prod instance
resource "aws_instance" "prod" {
  ami                     = var.amis[var.aws_region]
  instance_type           = "t2.micro"
  subnet_id               = aws_subnet.main_public.id
  vpc_security_group_ids  = [aws_security_group.dev_web.id]
  key_name                = aws_key_pair.my_key.key_name
}

# create subnet group
resource "aws_db_subnet_group" "mariadb_subnet" {
  name              = "mariadb_subnet_subnet"
  subnet_ids        = [aws_subnet.main_private.id, aws_subnet.main_private_1.id]
}

# generate password for RDS
resource "random_password" "rds_password" {
  length            = 16
  special           = false
}

#create RDS instance
resource "aws_db_instance" "myapp" {
  allocated_storage       = 10
  instance_class          = "db.t2.micro"
  engine                  = "mariadb"
  identifier              = "mariadb"
  name                    = "myapp_db"
  username                = "root_user"
  password                = random_password.rds_password.result
  db_subnet_group_name    = aws_db_subnet_group.mariadb_subnet.name
  availability_zone       = aws_subnet.main_private.availability_zone
  multi_az                = "false"
  vpc_security_group_ids  = [aws_security_group.allow_mariadb.id]
  skip_final_snapshot     = true
}
