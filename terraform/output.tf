output "ecr_url" {
  value = aws_ecr_repository.myapp.repository_url
}

output "prod_ip" {
  value = aws_instance.prod.public_ip
}

output "password" {
  value = aws_db_instance.myapp.password
}

output "endpoint" {
  value = aws_db_instance.myapp.endpoint
}