variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_region" {
  default = "eu-central-1"
}

variable "amis" {
  type = map(string)
  default = {
    eu-central-1 = "ami-0badcc5b522737046"
  }
}

variable "aws_profile" {
  default = "default"
}

variable "ssh_port" {
  default = 22
}

variable "web_port" {
  default = 80
}

variable "rds_port" {
  default = 3306
}


variable "PATH_TO_PRIVATE_KEY" {
  default = "C:\\Users\\User/.ssh/id_rsa"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "C:\\Users\\User/.ssh/id_rsa.pub"
}
